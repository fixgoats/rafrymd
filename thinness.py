#!usr/env/bin python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import scipy
import scipy.constants as cte

def getLims(xvals, yvals, pad):
    #Gefur mörk grafs á forminu [xmin, xmax, ymin, ymax].
    lims = np.zeros(4)
    xbil = np.abs(max(xvals) - min(xvals))
    ybil = np.abs(max(yvals) - min(yvals))
    lims[0] = min(xvals) - pad*xbil
    lims[1] = max(xvals) + pad*xbil
    lims[2] = min(yvals) - pad*ybil
    lims[3] = max(yvals) + pad*ybil
    return lims

show = True
d1N4148_rvs = np.loadtxt('1nrvs.txt', delimiter = '\t', skiprows = 1)
dbla_rvs = np.loadtxt('blarvs.txt', delimiter = '\t', skiprows = 1)
dbys_rvs = np.loadtxt('bysrvs.txt', delimiter = '\t', skiprows = 1)
dbz_rvs = np.loadtxt('bzrvs.txt', delimiter = '\t', skiprows = 1)
d1N4148_rvs = np.loadtxt('1nrvs.txt', delimiter = '\t', skiprows = 1)
dbla_rvs = np.loadtxt('blarvs.txt', delimiter = '\t', skiprows = 1)
dbys_rvs = np.loadtxt('bysrvs.txt', delimiter = '\t', skiprows = 1)
dbz_rvs = np.loadtxt('bzrvs.txt', delimiter = '\t', skiprows = 1)

V = 0
A = 1

fig1, ax1 = plt.subplots()
ax1.scatter(d1N4148_rvs[1:,V], 1e9*d1N4148_rvs[1:,A], marker = '+', label = '1N4148', c = 'g')
lims1 = getLims(d1N4148_rvs[1:,V], 1e9*d1N4148_rvs[1:,A], 0.05)
ax1.set_ylim([lims1[2], lims1[3]])
ax1.set(xlabel = '$V_r\\,[\\si{\\volt}]$', ylabel = '$I$ [$\\si{\\nano\\ampere}$]')
ax1.legend()
fig1.savefig('1N4148rvs.pdf')
if show:
    fig1.show()

fig2, ax2 = plt.subplots()
ax2.scatter(dbla_rvs[:,V], 1e6*dbla_rvs[:,A], marker = '+', label = 'Blár ljómtvistur', c = 'b')
ax2.set(xlabel = '$V_r\\,[\\si{\\volt}]$', ylabel = '$I$ [$\si{\\micro\\ampere}$]')
lims2 = getLims(dbla_rvs[:,V], 1e6*dbla_rvs[:,A], 0.05)
ax2.set_ylim([lims2[2], lims2[3]])
ax2.legend()
fig2.savefig('BlueLEDrvs.pdf')
if show:
    fig2.show()

fig3, ax3 = plt.subplots()
ax3.scatter(dbys_rvs[1:,V], 1e6*dbys_rvs[1:,A], marker = '+', label = 'BYS26-45', c = 'orange')
ax3.set(xlabel = '$V_r\\,[\\si{\\volt}]$', ylabel = '$I$ [$\\si{\\micro\\ampere}$]')
lims3 = getLims(dbys_rvs[1:,V], 1e6*dbys_rvs[1:,A], 0.05)
ax3.set_ylim([lims3[2], lims3[3]])
ax3.legend()
fig3.savefig('BYS26-45rvs.pdf')
if show:
    fig3.show()

fig4, ax4 = plt.subplots()
ax4.scatter(dbz_rvs[:,V], 1000*dbz_rvs[:,A], marker = '+', label = 'BZX-55C13', c = 'purple')
ax4.set(xlabel = '$V_r\\,[\\si{\\volt}]$', ylabel = '$I$ [$\\si{\\milli\\ampere}$]')
lims4 = getLims(dbz_rvs[:,V], 1000*dbz_rvs[:,A], 0.1)
ax4.set_ylim([lims4[2], lims4[3]])
ax4.legend()
fig4.savefig('BZX-55C13rvs.pdf')
if show:
    fig4.show()

if show:
    input()
