import sys
import numpy as np 
import matplotlib.pyplot as plt
from matplotlib import rcParams

#fastar
q = 1.602E-19
K_s = 11.8
eps_0 = 8.85e-12
d = 0.003
A = d**2*np.pi/4



# FYRIR BÖKUN

deig = np.loadtxt('Gb1.dat', skiprows=8)
Vdeig = -deig[:,0]
Cdeig = deig[:,2]

Wdeig = K_s*eps_0*A*Cdeig**(-1)*1e6
N_Deig = 2*Cdeig**2*Vdeig*(q*K_s*eps_0*A**2)**(-1)*1e-18

fig1, ax1 = plt.subplots()

ax1.scatter(Wdeig, N_Deig)

ax1.set(xlabel = 'Dýpt ' + r'\([\mu m]\)', ylabel = 'Íbótarþéttleiki, ' +
        r'\(N_D\ [\mu m^{-3}]\)')
fig1.show()
print("Vista mynd? [Y,n]")
t = input()
if "n" not in t:
    fig1.savefig('Gb1.pdf')
plt.close(fig1)

# EFTIR 393 K BÖKUN
        
bread = np.loadtxt('Gb2.dat', skiprows=8)
Vbread = -bread[:,0]
Cbread = bread[:,2]

Wbread = K_s*eps_0*A*Cbread**(-1)*1e6
N_Dread = 2*Cbread**2*Vbread*(q*K_s*eps_0*A**2)**(-1)*1e-18

fig2, ax2 = plt.subplots()

ax2.scatter(Wbread, N_Dread)

ax2.set(xlabel = 'Dýpt ' + r'\([\mu m]\)')
ax2.set(ylabel = 'Íbótarþéttleiki, ' + r'\(N_D\ [\mu m^{-3}]\)')
fig2.show()
print("Vista mynd? [Y,n]")
t = input()
if "n" not in t:
    fig2.savefig('Gb2.pdf')

plt.close(fig2)

# EFTIR 423 K BÖKUN

biscuit = np.loadtxt('Gb3.dat', skiprows=8)
Vbisc = -biscuit[:,0]
Cbisc = biscuit[:,2]

Wbisc = K_s*eps_0*A*Cbisc**(-1)*1e6
N_Driscuit = 2*Cbisc**2*Vbisc*(q*K_s*eps_0*A**2)**(-1)*1e-18

fig3, ax3 = plt.subplots()

ax3.scatter(Wbisc, N_Driscuit)

ax3.set(xlabel = 'Dýpt ' + r'\([\mu m]\)')
ax3.set(ylabel = 'Íbótarþéttleiki, ' + r'\(N_D\ [\mu m^{-3}]\)')
fig3.show()
print("Vista mynd? [Y,n]")
t = input()
if "n" not in t:
    fig3.savefig('Gb3.pdf')

plt.close(fig3)
