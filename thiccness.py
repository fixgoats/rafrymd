#!usr/env/bin python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import scipy
import scipy.constants as cte

def getLims(xvals, yvals):
    #Gefur mörk grafs á forminu [xmin, xmax, ymin, ymax].
    lims = np.zeros(4)
    xbil = np.abs(max(xvals) - min(xvals))
    ybil = np.abs(max(yvals) - min(yvals))
    lims[0] = min(xvals) - 0.05*xbil
    lims[1] = max(xvals) + 0.05*xbil
    lims[2] = min(yvals) - 0.05*ybil
    lims[3] = max(yvals) + 0.05*ybil
    return lims

show = True
d1N4148_fwd = np.loadtxt('1nfwd.txt', delimiter = '\t', skiprows = 1)
dbla_fwd = np.loadtxt('blafwd.txt', delimiter = '\t', skiprows = 1)
dbys_fwd = np.loadtxt('bysfwd.txt', delimiter = '\t', skiprows = 1)
dbz_fwd = np.loadtxt('bzfwd0.txt', delimiter = '\t', skiprows = 1)
d1N4148_rvs = np.loadtxt('1nrvs.txt', delimiter = '\t', skiprows = 1)
dbla_rvs = np.loadtxt('blarvs.txt', delimiter = '\t', skiprows = 1)
dbys_rvs = np.loadtxt('bysrvs.txt', delimiter = '\t', skiprows = 1)
dbz_rvs = np.loadtxt('bzrvs.txt', delimiter = '\t', skiprows = 1)

V = 0
A = 1

N41V = d1N4148_fwd[(d1N4148_fwd[:,0] > 0.1) & (d1N4148_fwd[:,0] < 0.8), 0]
N41A = np.log(d1N4148_fwd[(d1N4148_fwd[:,0] > 0.1) & (d1N4148_fwd[:,0] < 0.8), 1])
N41a, N41b = np.polyfit(N41V, N41A, deg = 1)
print('1N4148:')
print(str(N41a) + 'x + ' + str(N41b))
fig1, ax1 = plt.subplots()
ax1.scatter(d1N4148_fwd[:,V], np.log(d1N4148_fwd[:,A]), marker = '+', label = '1N4148', c = 'g')
ax1.plot([0.1,0.8], [0.1*N41a + N41b, 0.8*N41a + N41b], c = 'orange')
ax1.set(xlabel = '$V_A\\,[\\si{\\volt}]$', ylabel = '$\ln I$')
ax1.legend()
fig1.savefig('1N4148log.pdf')
if show:
    fig1.show()
N41 = cte.e/(N41a*cte.k*298)
print('Gæðastuðull 1N4148 er n = ' + str(N41))
blaV1 = dbla_fwd[(dbla_fwd[:,0] > 1.8) & (dbla_fwd[:,0] < 2.2), 0]
blaA1 = np.log(dbla_fwd[(dbla_fwd[:,0] > 1.8) & (dbla_fwd[:,0] < 2.2), 1])
blaV2 = dbla_fwd[(dbla_fwd[:,0] > 2.3) & (dbla_fwd[:,0] < 2.55), 0]
blaA2 = np.log(dbla_fwd[(dbla_fwd[:,0] > 2.3) & (dbla_fwd[:,0] < 2.55), 1])
bla1a, bla1b = np.polyfit(blaV1, blaA1, deg = 1)
print('Blár ljómtvistur fyrri hluti:')
print(str(bla1a) + 'x + ' + str(bla1b))
bla1 = cte.e/(bla1a*cte.k*298)
bla2a, bla2b = np.polyfit(blaV2, blaA2, deg = 1)
print('Blár ljómtvistur seinni hluti:')
print(str(bla2a) + 'x + ' + str(bla2b))
bla2 = cte.e/(bla2a*cte.k*298)
fig2, ax2 = plt.subplots()
ax2.scatter(dbla_fwd[:,V], np.log(dbla_fwd[:,A]), marker = '+', label = 'Blár ljómtvistur', c = 'b')
ax2.plot([1.8, 2.2], [1.8*bla1a + bla1b, 2.2*bla1a + bla1b], c = 'yellow')
ax2.plot([2.3, 2.55], [2.3*bla2a + bla2b, 2.55*bla2a + bla2b], c = 'orange')
ax2.set(xlabel = '$V_A\\,[\\si{\\volt}]$', ylabel = '$\ln I$')
ax2.legend()
fig2.savefig('BlueLEDlog.pdf')
if show:
    fig2.show()
print('Gæðastuðull bláa ljómtvistsins er n = ' + str(bla1) + ' og n = ' + str(bla2))
bysV = dbys_fwd[(dbys_fwd[:,0] > 0.05) & (dbys_fwd[:,0] < 0.26), 0]
bysA = np.log(dbys_fwd[(dbys_fwd[:,0] > 0.05) & (dbys_fwd[:,0] < 0.26), 1])
bysa, bysb = np.polyfit(bysV, bysA, deg = 1)
print('BYS26-45:')
print(str(bysa) + 'x + ' + str(bysb))
bys = cte.e/(bysa*cte.k*298)
fig3, ax3 = plt.subplots()
ax3.scatter(dbys_fwd[:,V], np.log(dbys_fwd[:,A]), marker = '+', label = 'BYS26-45', c = 'orange')
ax3.plot([0.05, 0.26], [0.05*bysa + bysb, 0.26*bysa + bysb], c = 'blue')
ax3.set(xlabel = '$V_A\\,[\\si{\\volt}]$', ylabel = '$\ln I$')
ax3.legend()
fig3.savefig('BYS26-45log.pdf')
if show:
    fig3.show()
print('Gæðastuðull BYS26-45 er n = ' + str(bys))
bzxV = dbz_fwd[(dbz_fwd[:,0] > 0.2) & (dbz_fwd[:,0] < 0.8), 0]
bzxA = np.log(dbz_fwd[(dbz_fwd[:,0] > 0.2) & (dbz_fwd[:,0] < 0.8), 1])
bzxa, bzxb = np.polyfit(bzxV, bzxA, deg = 1)
print('BZX-55C13:')
print(str(bzxa) + 'x + ' + str(bzxb))
bzx = cte.e/(bzxa*cte.k*298)
fig4, ax4 = plt.subplots()
ax4.scatter(dbz_fwd[:,V], np.log(dbz_fwd[:,A]), marker = '+', label = 'BZX-55C13', c = 'purple')
ax4.plot([0.2, 0.8], [0.2*bzxa + bzxb, 0.8*bzxa + bzxb], c = 'orange')
ax4.set(xlabel = '$V_A\\,[\\si{\\volt}]$', ylabel = '$\ln I$')
ax4.legend()
fig4.savefig('BZX-55C13log.pdf')
if show:
    fig4.show()
print('Gæðastuðull BZX-55C13 er n = ' + str(bzx))

if show:
    input()

